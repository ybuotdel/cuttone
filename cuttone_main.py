#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2020 Yorick Buot de l'Epine

#
# Cuttone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.
#

import wavio
import numpy 
import os.path
import os
os.environ['ETS_TOOLKIT'] = 'qt5'
os.environ['QT_API'] = 'pyqt'
####################################
from PyQt5 import QtGui,QtCore
from PyQt5.QtCore import QObject, pyqtSignal
import pyqtgraph as pg
import pyaudio
import cuttone_gui2
import param_inverse
import wave
import psutil, os

def kill_proc_tree(pid, including_parent=True):    
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()


class plot_sig:
    def __init__(self,p):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground((235,235,235))
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.courbe=self.p.plot()
        self.courbe.setData(numpy.linspace(0,50,50),numpy.zeros((50)),pen=pg.mkPen('b',width=2))
        self.p.setXRange(0,50)
        self.p.setYRange(-1.1,1.1)
        #mise en forme axe#######
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (V)')
        self.p.setLabel('bottom',text ='Temps (s)')
        #ajout des lignes de visualisation 
        self.vLine = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen('y',width=1.5))
        self.vLine2 = pg.InfiniteLine(angle=90, movable=True,pen=pg.mkPen('y',width=1.5))
        self.p.addItem(self.vLine, ignoreBounds=True)
        self.p.addItem(self.vLine2, ignoreBounds=True)
        self.reset()
    def reset(self):
#        x_min=(self.p.getViewBox().viewRange()[0][0])
        x_max=(self.p.getViewBox().viewRange()[0][1])
        self.vLine.setValue(0.1*(x_max))
        self.vLine2.setValue(0.9*(x_max))
        self.update_cross()
    def update_cross(self):
        x_min=0.98*(self.p.getViewBox().viewRange()[0][0])
        x_max=0.98*(self.p.getViewBox().viewRange()[0][1])
        x=self.vLine.value()
        x2=self.vLine2.value()
        if x<x_min:
            self.vLine.setValue(x_min)
        if x2>x_max:
            self.vLine2.setValue(x_max)  
        if x2<x:
            self.vLine2.setValue(x+0.05*x_max)
        if x>x2:
            self.vLine.setValue(x-0.05*x_max)
    def update_data(self,X,Y):          
         self.courbe.setData(X,Y,pen=pg.mkPen(color='b',width=2),autoDownsample=True)        
         self.p.setXRange(0,X[-1])
         self.p.setYRange(numpy.amin(Y),numpy.amax(Y))
         
    def suppr(self):
        self.p.clear()



class plot_spectrum:
    def __init__(self,p,var):
        pen = pg.mkPen(color='k', width=2)
        self.p = p
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.p.setBackground((235,235,235))
#        colortable=color_table()
        colortable=cubehelix()
        self.image=pg.ImageItem(setAutoDownsample=True)
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        Xaxis.enableAutoSIPrefix(False)
        Xaxis.enableAutoSIPrefix(False)
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*numpy.size(var.data_freq)/ntick))  
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*numpy.size(var.data_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        self.p.setLabel('left', text ='<font size="4">Frequency </font>', units ='<font size="4">Hz </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time </font>', units ='<font size="4">Seconds </font>')     

        
    def reset(self,var):
        self.p.removeItem(self.image)
        self.p.removeItem(self.p.getPlotItem().getAxis('bottom'))
        self.p.removeItem(self.p.getPlotItem().getAxis('left'))
#        colortable=color_table()
        colortable=cubehelix()
        self.image=pg.ImageItem(setAutoDownsample=True)
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*numpy.size(var.data_freq)/ntick))  
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*numpy.size(var.data_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        ##########################- define cross ###################
        self.image.setZValue(-1) #◙remet l'image à l'arrière plan
        
    def update_data(self,mat,db_min,db_max):          
         self.image.setImage(mat,autoDownSample=True)  
         self.image.setLevels([db_min,db_max])    

class Variable:
    def __init__(self):
        self.freq_max = [] #max frequency to study
        self.buffer_size = [] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] #covering for windowed
        self.channel=[] # voie
        self.time_max=[]      
        self.data_time=[]
        self.data_freq=[]
        self.nb_cover=[]
        self.db_min=[]
        self.db_max=[]
        self.db_min_old=[]
        self.db_max_old=[]
        self.roi=[]
        self.roi_val=[]
        self.factor_rate=2.0
        self.signal=[]
        self.signal_time=[]
        self.signal_rate=[]

class signal_BUFF:
    def init(self,signal_size):
        self.data=numpy.zeros((signal_size))
    def add(self,x):
        self.data[0:numpy.size(x)]=x
        self.data=numpy.roll(self.data,-numpy.size(x))

class spectro_BUFF:
    def init(self,spectro_size):
        self.data=1e-7*numpy.ones((spectro_size),'complex')
    def add(self,x,n):
        self.data[:,0:n]=x
        self.data=numpy.roll(self.data,-n)
        self.data[-1,:]=1e-7
    def abs_data(self):
        return numpy.abs(self.data)

def fenetre(weight_hanning,bloc):
    bloc_out=numpy.multiply(weight_hanning,bloc)
    return bloc_out    
    
    
    ############Î definie la table des couleurs
def cubehelix(gamma=1, s=0.5, r=-1.5, h=1.0):
    def get_color_function(p0, p1):
        def color(x):
            xg = x ** gamma
            a = h * xg * (1 - xg) / 2
            phi = 2 * numpy.pi * (s / 3 + r * x)
            return xg + a * (p0 * numpy.cos(phi) + p1 * numpy.sin(phi))
        return color
    array = numpy.empty((256, 3))
    abytes = numpy.arange(0, 1, 1/256.)
    array[:, 0] = get_color_function(-0.14861, 1.78277)(abytes) * 255
    array[:, 1] = get_color_function(-0.29227, -0.90649)(abytes) * 255
    array[:, 2] = get_color_function(1.97294, 0.0)(abytes) * 255
    return array
#
#def rainbow():
#    lut = numpy.empty((256, 3))
#    abytes = numpy.arange(0, 1, 0.00390625)
#    lut[:, 0] = numpy.abs(2 * abytes - 0.5) * 255
#    lut[:, 1] = numpy.sin(abytes * numpy.pi) * 255
#    lut[:, 2] = numpy.cos(abytes * numpy.pi / 2) * 255
#    return lut
    
def color_table():
#    pos = numpy.array([0.0, 0.5, 1.0])
    pos = numpy.array([0.0, 1.0])
#    color = numpy.array([[0,0,0,255], [87,108,128,255], [175,215,255,255]], dtype=numpy.ubyte)
    color = numpy.array([[0,0,0,255],[0,255,255,255]], dtype=numpy.ubyte)
    map = pg.ColorMap(pos, color)
    lut = map.getLookupTable(0.0, 1.0, 256)
    return lut
    
def color_roi():
    pos = numpy.array([0.0, 0.5, 1.0])
    color = numpy.array([[255,0,0,255], [255,255,255,255], [0,255,0,255]], dtype=numpy.ubyte)
    map = pg.ColorMap(pos, color)
    lut = map.getLookupTable(0.0, 1.0, 256)
    return lut

def colormap_gui(fig,min_val,max_val):
    pen = pg.mkPen(color='k', width=2)
    nb_color=255
    value=numpy.array([numpy.linspace(min_val,max_val,nb_color),numpy.linspace(min_val,max_val,nb_color)])
    colortable=cubehelix()
#    colortable=rainbow()
   # colortable=color_table()
    fig.clear()       
    fig.setBackground((235.0,235.0,235.0))
    fig.plotItem.getAxis('left').setPen(pen)
    fig.plotItem.getAxis('bottom').setPen(pen)
    image=pg.ImageItem()
    item_color=fig.getPlotItem()
    image.setLookupTable(colortable)
    fig.addItem(image)
    image.setImage(value,autoDownSample=True) 
    image.setLevels([min_val,max_val])
    Xaxis=item_color.getAxis('bottom')
    Yaxis=item_color.getAxis('left')
    ntick=7
    Ytick_loc=[]
    Ytick_string=[]
    for ii in range(0,ntick+1):
        Ytick_string.append("{0:.1f}".format(round(min_val+(ii*((max_val-min_val)/float(ntick))))))
        Ytick_loc.append(round((ii*((nb_color)/float(ntick)))))
    Ytick=zip(Ytick_loc,Ytick_string)
    Yaxis.setTicks([list(Ytick)])
    Xaxis.hide()
    
def write_spectro(fid,data,freq,time):
    shape=numpy.shape(data)
    for ii in range(0,shape[1]):
        fid.write("{0:.3f}".format(time[ii])+';')
    fid.write('\n')
    for jj in range(0,shape[0]):
        fid.write("{0:.3f}".format(freq[jj])+';')
        for ii in range(0,shape[1]):
            fid.write("{0:.3f}".format(data[jj,ii])+';')
        fid.write('\n')
#    fid.close()
    
class splashScreen:
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash_cuttone.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint |
                            QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix))                     # 10 = Windowrole
        self.splash.setPalette(palette)
        
    def show_splash(self):
        self.splash.show()
    def close_splash(self):
        self.splash.close()
           
class param_window(QtGui.QWidget,param_inverse.Ui_param):
    def __init__(self, parent=None):
        super(param_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Parameter")    

class spectro(QtGui.QMainWindow,cuttone_gui2.Ui_MainWindow):
    def __init__(self, parent=None):
        super(spectro, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Cuttone") 
        ############### definie la fenetre des paramètre ########
        self.param = param_window()
        ############# initalise variable ###################
        os.chdir('C:\\')
        self.acqui=0
        self.variable=Variable()
        self.voie_box.addItems(['Left','Right'])
        self.voie_box.setCurrentIndex(0)
        self.define_channel()
        self.param.freq_edit.insert('10000')
        self.variable.freq_max = int(self.param.freq_edit.text()) #max frequency to study
        self.param.line_box.addItems(['100','200','400','800','1600','3200'])
        self.param.line_box.setCurrentIndex(2)
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.param.fenetre_box.addItems(['Uniform','Hanning'])
        self.param.fenetre_box.setCurrentIndex(1)
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.param.recouvrement_edit.insert('0.5')
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.define_delta_f() # définie le delta f
        self.attenuation_edit.insert('-20')
        self.rate_edit.clear()
        self.rate_edit.setEnabled(False)
        #############################################
        self.min_db_edit.insert('-80')
        self.max_db_edit.insert('0')
        self.min_db_old_edit.insert('-80')
        self.max_db_old_edit.insert('0')
        ############# inti varibale for initiale plot #############################
        self.define_colormap()
        self.define_colormap_old()
        self.signal_plot=plot_sig(self.plot_signal)
        self.define_data_freq_time()
        self.spectro_plot=plot_spectrum(self.plot_spectro,self.variable)
        self.spectro_plot_ini=plot_spectrum(self.plot_spectro_ini,self.variable)
        ###################################
        self.generate_button.setEnabled(False)
        self.start_button.setEnabled(False)
        self.listen_old_button.setEnabled(False)
        self.listen_button.setEnabled(False)
        self.export_button.setEnabled(False)
        self.export_old_button.setEnabled(False)
        self.add_button.setEnabled(False)
        self.wave_button.setEnabled(False)
        delta_f=self.variable.delta_freq# equivalant au nombre de buffer à remplir par seconde
        self.variable.nb_cover=int(round(1/(1-self.variable.covering)))
        self.variable.data_freq=numpy.arange(0,self.variable.freq_max,delta_f)#[0:-1]
        self.define_data_freq_time()
        self.variable.time_max=30
        self.variable.data_time=numpy.linspace(0,self.variable.time_max,
                                               round(self.variable.nb_cover*self.variable.time_max*delta_f))
#        ################## créer les buffers ##############################################
#        ###### Signal audio pour toute la duree de l'enregistrement #####
        self.signal_buffer=signal_BUFF()
        self.signal_buffer.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        self.signal_buffer_new=signal_BUFF()
        self.signal_buffer_new.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
#        ###################### spectre sur toute la durrée de l'enregistrement + nouveau spectre generé ###########
        self.spectro_buff=spectro_BUFF()
        self.spectro_buff.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 
        self.spectro_buff_new=spectro_BUFF()
        self.spectro_buff_new.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 
        ######### init fig ############################################################
        self.define_data_freq_time()  
        #################################################
        ################################################### 
        
        #################################################☺
        self.connectActions()
         
    def connectActions(self):
        self.param.enregistrement_button.clicked.connect(self.save_param)
        self.param.freq_edit.editingFinished.connect(self.define_delta_f)
        self.param.line_box.currentIndexChanged.connect(self.define_delta_f)
        self.param.fenetre_box.currentIndexChanged.connect(self.define_window)
    ##############################################################################
        self.import_button.clicked.connect(self.import_wave)
        self.option_button.clicked.connect(self.param_open)
        self.start_button.clicked.connect(self.main)
        self.listen_old_button.clicked.connect(self.listen_sig_old)
        self.listen_button.clicked.connect(self.listen_sig)
        self.wave_button.clicked.connect(self.save_wave)
        self.voie_box.currentIndexChanged.connect(self.define_channel)
        self.max_db_edit.returnPressed.connect(self.define_colormap)
        self.min_db_edit.returnPressed.connect(self.define_colormap)
        self.max_db_old_edit.returnPressed.connect(self.define_colormap_old)
        self.min_db_old_edit.returnPressed.connect(self.define_colormap_old)
        self.export_button.clicked.connect(self.export_spectro_FFT)
        self.export_old_button.clicked.connect(self.export_spectro_FFT_old)
        self.add_button.clicked.connect(self.add_roi)
        self.undo_button.clicked.connect(self.undo)
        self.generate_button.clicked.connect(self.generate)
        self.signal_plot.vLine.sigDragged.connect(lambda: self.signal_plot.update_cross())
        self.signal_plot.vLine2.sigDragged.connect(lambda: self.signal_plot.update_cross())


    def param_open(self):
        """Lance la deuxième fenêtre"""
        self.param.show() 
        self.main_stop()
        
    def import_wave(self):
        filenom=QtGui.QFileDialog.getOpenFileName(self,filter ='wav (*.wav *.)')
        filename=filenom[0]
        try:
            temp=wave.open(filename,'rb')
            time=numpy.linspace(0,float(temp.getnframes())/temp.getframerate(),temp.getnframes())
            self.variable.signal_time=time
            self.variable.signal_rate=float(temp.getnframes())/self.variable.signal_time[-1]
            self.variable.signal=numpy.zeros((temp.getnframes(),2))
            #################################
            sig=wavio.read(filename)
            nb_bit=[8,16,24,38]
            bit=nb_bit[sig.sampwidth-1]            
            if temp.getnchannels()==1:
                #on sauvegarde le signal sur les deux voies
                temp_sig=(sig.data[:,0]/float(2**(bit-1)))
                delta=0
                if numpy.amax(temp_sig)>1:
                    delta=1
                self.variable.signal[:,0]=temp_sig-delta
                self.variable.signal[:,1]=temp_sig-delta
            if temp.getnchannels()==2:
                temp_sig=(sig.data[:,0]/float(2**(bit-1)))
                delta=0
                if numpy.amax(temp_sig)>1:
                    delta=1
                self.variable.signal[:,0]=temp_sig-delta
                temp_sig=(sig.data[:,1]/float(2**(bit-1)))
                self.variable.signal[:,1]=temp_sig-delta
            self.signal_plot.update_data(self.variable.signal_time,self.variable.signal[:,self.variable.channel])
            self.signal_plot.reset()
            self.rate_edit.clear()
            self.rate_edit.insert("{0:.1f}".format(self.variable.signal_rate))
            ##################### on active les bouton #############
            self.start_button.setEnabled(True)
        except:
            filename=QtGui.QFileDialog.getOpenFileName(self,filter ='wav (*.wav *.)')      
      
    def save_param(self):
        self.define_channel()
        self.variable.freq_max = int(self.param.freq_edit.text()) #max frequency to study
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.define_data_freq_time()
        ########♣ remet les slide au milieu des plage de frequence et de temps
        self.param.close()
#        disp(self.signal_buffer.data)
#        disp(self.spectro_buff.data)
##        disp(self.variable.buffer_size)
##        disp(self.variable.freq_max)
##        disp(self.variable.data_freq)
##        disp(self.variable.data_time)
        #######################################
        
        
    
        
    def define_data_freq_time(self):
        delta_f=self.variable.delta_freq# equivalant au nombre de buffer à remplir par seconde
        self.variable.nb_cover=int(round(1/(1-self.variable.covering)))
        self.variable.data_freq=numpy.arange(0,self.variable.freq_max,delta_f)#[0:-1]
        t1=self.signal_plot.vLine.value()
        t2=self.signal_plot.vLine2.value()
        self.variable.time_max=numpy.abs(t2-t1)
        self.variable.data_time=numpy.linspace(0,self.variable.time_max,int(round(self.variable.nb_cover*self.variable.time_max*delta_f)))
        ################## créer les buffers ##############################################
        ###### Signal audio pour toute la duree de l'enregistrement #####
#        ##################################################################################        
        self.signal_bufferG=signal_BUFF()
        self.signal_bufferG.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        self.signal_bufferG_new=signal_BUFF()
        self.signal_bufferG_new.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        self.signal_bufferD=signal_BUFF()
        self.signal_bufferD.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        self.signal_bufferD_new=signal_BUFF()
        self.signal_bufferD_new.init(int(self.variable.time_max*delta_f*self.variable.buffer_size))
        ###################### spectre sur toute la durrée de l'enregistrement + nouveau spectre generé ###########
        self.spectro_buffG=spectro_BUFF()
        self.spectro_buffG.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 
        self.spectro_buffG_new=spectro_BUFF()
        self.spectro_buffG_new.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 
        self.spectro_buffD=spectro_BUFF()
        self.spectro_buffD.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 
        self.spectro_buffD_new=spectro_BUFF()
        self.spectro_buffD_new.init((numpy.size(self.variable.data_freq),numpy.size(self.variable.data_time))) 


    def define_channel(self):
        voie=self.voie_box.currentText()
        if voie=='Left':
            self.variable.channel=0
        if voie=='Right':
            self.variable.channel=1
        try:
            self.signal_plot.update_data(self.variable.signal_time,self.variable.signal[:,self.variable.channel])
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec le logarithm
            else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec 
            self.spectro_plot_ini.update_data(20*numpy.log10(data_plot.T),self.variable.db_min_old,self.variable.db_max_old)
        except:
            pass
            
        
    
    def define_window(self):
        fenetre=self.param.fenetre_box.currentText()
        if fenetre=='Uniform':
            self.param.recouvrement_edit.setText('0')
        if fenetre=='Hanning':
            self.param.recouvrement_edit.setText('0.66')
               
    def define_delta_f(self):
        self.param.enregistrement_button.setEnabled(True)
        freq_max=int(self.param.freq_edit.text())
        ####################################################################
#        if freq_max>=500 and freq_max<=9000:
        buffer =self.variable.factor_rate*int(self.param.line_box.currentText()) # size of buffer
        self.param.enregistrement_button.setEnabled(True)
        self.variable.delta_freq=int(self.variable.factor_rate*freq_max)/float(buffer)
        self.param.delta_label.setText("{0:.3f}".format(self.variable.delta_freq))

    
    def define_colormap(self):
        val_min=float(self.min_db_edit.text())
        val_max=float(self.max_db_edit.text())
        colormap_gui(self.plot_colorbar,val_min,val_max)
        self.variable.db_min=val_min
        self.variable.db_max=val_max
        ############# si pas d'acquisition, on replote le spectre du buffer #####
        if self.acqui==2:
            self.spectro_plot.reset(self.variable)
            if self.variable.channel==0:
                self.spectro_plot.update_data(20*numpy.log10(abs(self.spectro_buffG_new.data).T),self.variable.db_min,self.variable.db_max)
            else:
                self.spectro_plot.update_data(20*numpy.log10(abs(self.spectro_buffD_new.data).T),self.variable.db_min,self.variable.db_max)

    def define_colormap_old(self):
        val_min=float(self.min_db_old_edit.text())
        val_max=float(self.max_db_old_edit.text())
        colormap_gui(self.plot_colorbar_old,val_min,val_max)
        self.variable.db_min_old=val_min
        self.variable.db_max_old=val_max
        ############# si pas d'acquisition, on replote le spectre du buffer #####
        if self.acqui==1:
            self.spectro_plot_ini.reset(self.variable)
            if self.variable.channel==0:
                self.spectro_plot_ini.update_data(20*numpy.log10(abs(self.spectro_buffG.data).T),self.variable.db_min_old,self.variable.db_max_old)
            else:
                self.spectro_plot_ini.update_data(20*numpy.log10(abs(self.spectro_buffD.data).T),self.variable.db_min_old,self.variable.db_max_old)

    def add_roi(self):
        val_min=-25
        val_max=20
        p=self.plot_spectro_ini
        val_roi=int(self.attenuation_edit.text())
        if val_roi>val_max:
            val_roi=val_max
            self.attenuation_edit.clear()
            self.attenuation_edit.insert("{:d}".format(val_max))
        elif val_roi<val_min:
            val_roi=val_min
            self.attenuation_edit.clear()
            self.attenuation_edit.insert("{:d}".format(val_min))
        lut_color=color_roi()
        ind=int((255*float(val_roi-val_min)/(val_max-val_min)))
        # define bounding rectange for ROI bounding
        roi=pg.PolyLineROI([(50,50),(50,100),(100,100),(100,50)],closed=True,pen=pg.mkPen(color=lut_color[ind,:],width=2))     
        p.addItem(roi)
        self.variable.roi.append(roi)
        self.variable.roi_val.append(val_roi) 
    
    def undo(self):
        p=self.plot_spectro_ini
        p.removeItem(self.variable.roi[-1])
        del(self.variable.roi[-1])
        del(self.variable.roi_val[-1])
        
    def main(self):
        ############# intialisation du GUI
#        self.start_button.setEnabled(False)
#        self.stop_button.setEnabled(True)
        self.add_button.setEnabled(True)
        self.export_old_button.setEnabled(True)
        self.listen_old_button.setEnabled(True)
        self.generate_button.setEnabled(True)
        ################## delete existing roi ##########
        for ii in range(0,numpy.size(self.variable.roi)):
            self.undo()
        self.variable.roi=[]
        self.variable.roi_val=[]
        ########## intialisation des paramètre ########
        self.define_data_freq_time()
#        if self.variable.freq_max*self.variable.factor_rate>self.variable.signal_rate:
#            RATE=int(self.variable.factor_rate)
#        else:
        RATE=int(self.variable.factor_rate*self.variable.freq_max)
        BUFFER=self.variable.buffer_size
        if self.variable.window=='Hanning':
            weight=0.5-0.5*(numpy.cos(2*numpy.pi*numpy.arange(0,BUFFER)/BUFFER))
        if self.variable.window=='Uniform':
            weight=numpy.ones((self.variable.buffer_size))
        taux_overlap=self.variable.covering
        nb_cover=int(round(1/(1-taux_overlap)))
        ######### création des buffers et interpolation du signal pour 
        t1=self.signal_plot.vLine.value()
        t2=self.signal_plot.vLine2.value()
        if t1>t2:
            t=t1
            t1=t2
            t2=t
        ind1=numpy.where(self.variable.signal_time>t1)
        ind2=numpy.where(self.variable.signal_time<t2)
        ind=numpy.intersect1d(ind1,ind2)
        nb_point=int(round(numpy.size(self.variable.data_time)*(1-taux_overlap)*BUFFER))#numpy.size(self.variable.data_time)*(1-taux_overlap)*BUFFER+BUFFER
        if len(ind)>0: # verfie que l'on n'a bien un signal à resampler
            self.signal_bufferG.data=numpy.interp(numpy.linspace(t1,t2,nb_point),self.variable.signal_time[ind],self.variable.signal[ind,0])
            self.signal_bufferD.data=numpy.interp(numpy.linspace(t1,t2,nb_point),self.variable.signal_time[ind],self.variable.signal[ind,1])
        ######### determination lignes frequentielles et temproelles ######
            freq_data=self.variable.data_freq# ligne freq spectrograme
        ######### intialisation des ringbuffer et matrice vide #############
#            data_fft=1e-7*numpy.ones((BUFFER),'complex')
            self.spectro_plot_ini.reset(self.variable)
            if self.variable.channel==0:
                self.spectro_plot_ini.update_data(20*numpy.log10(abs(self.spectro_buffG.data).T),self.variable.db_min_old,self.variable.db_max_old)
            else:
                self.spectro_plot_ini.update_data(20*numpy.log10(abs(self.spectro_buffD.data).T),self.variable.db_min_old,self.variable.db_max_old)
            for j in range(0,numpy.size(self.variable.data_time)-2):   
                ind_wind= numpy.arange(j*(1-taux_overlap)*BUFFER,j*(1-taux_overlap)*BUFFER+BUFFER).astype(int)
                sig=numpy.zeros((numpy.size(ind_wind),2))
                try:# test pour voir si on a un nombre de point dans le restant du signal suffisant pour faire une FFT
                    dataG=self.signal_bufferG.data[ind_wind]
                    dataD=self.signal_bufferD.data[ind_wind]
                except:
                    dataG=numpy.zeros(BUFFER)
                    tempG=self.signal_bufferG.data[-BUFFER::]
                    dataG=tempG
                    dataD=numpy.zeros(BUFFER)
                    tempD=self.signal_bufferD.data[-BUFFER::]
                    dataD=tempD
                sig[0:numpy.size(ind_wind),0]=dataG
                sig[0:numpy.size(ind_wind),1]=dataD
                windowed_signalG=fenetre(weight,sig[0:BUFFER,0])
                windowed_signalD=fenetre(weight,sig[0:BUFFER,1])
#                windowed_signal=self.signal_buffer.data[ind_wind]
                data_fft=numpy.fft.fft(windowed_signalG,BUFFER)/(BUFFER)
                data_fft[0]=0.5*data_fft[0]
                self.spectro_buffG.data[:,j+1]=2*numpy.array(data_fft[0:numpy.size(freq_data)])
                ######################################################################
                data_fft=numpy.fft.fft(windowed_signalD,BUFFER)/(BUFFER)
                data_fft[0]=0.5*data_fft[0]
                self.spectro_buffD.data[:,j+1]=2*numpy.array(data_fft[0:numpy.size(freq_data)])
            if self.variable.channel==0:
                data_plot=abs(self.spectro_buffG.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec le logarithm
            else:
                data_plot=abs(self.spectro_buffD.data)
                data_plot[data_plot==0]=1e-7#evite les cas infinie avec le logarithm
            self.spectro_plot_ini.update_data(20*numpy.log10(data_plot.T),self.variable.db_min_old,self.variable.db_max_old)
                ########################################################################
            self.start_button.setEnabled(True)
            self.add_button.setEnabled(True)
            self.export_old_button.setEnabled(True)
            self.listen_old_button.setEnabled(True)             
                ##########################################################################
            app.processEvents()
            self.acqui=1
    
    def generate(self):
        ###########################################
        #  constrcution du nouveau tenmps frequence
        ###########################################
        self.export_button.setEnabled(True)
        self.listen_button.setEnabled(True)
        self.wave_button.setEnabled(True)
        self.spectro_plot.reset(self.variable)
        modif_spectro=numpy.ones(numpy.shape(self.spectro_buffG.data))
        for ii in range(0,numpy.size(self.variable.roi)):
            point=self.variable.roi[ii].getLocalHandlePositions()
            pos=self.variable.roi[ii].pos()
            k=int(numpy.size(point)/2.0)
            points=[]
            #### verification et réecriture des points dans le repere d'origine(0,0) par rapport au spectrographe
            for jj in range(0,k):
                X=point[jj][1].x()
                Y=point[jj][1].y()
                if pos.x()+X<0:
                    X=0
                elif pos.x()+X>numpy.size(self.variable.data_time):
                    X=numpy.size(self.variable.data_time)
                else:
                    X=X+pos.x()
                if pos.y()+Y<0:
                    Y=0
                elif pos.y()+Y>numpy.size(self.variable.data_freq):
                    Y=numpy.size(self.variable.data_freq)
                else:
                    Y=Y+pos.y()
                points.append((X,Y))
            self.variable.roi[ii].setPos(QtCore.QPoint(0,0))
            self.variable.roi[ii].clearPoints()
            self.variable.roi[ii].setPoints(points,closed=True)
            val_roi=self.variable.roi_val[ii]
            val=self.variable.roi[ii].getArrayRegion(numpy.transpose(self.spectro_buffG.data),self.spectro_plot_ini.image)#inverser car la matrice de buff l'est aussi
            ind=numpy.ones(numpy.shape(val))
            ind[numpy.nonzero(numpy.abs(val))]=10**(float(val_roi)/20)
            size=numpy.shape(ind)
            pos=self.variable.roi[ii].pos()
            x=self.variable.roi[ii].boundingRect().x()+pos.x()
            y=self.variable.roi[ii].boundingRect().y()+pos.y()
            modif_spectro[int(y):int(y)+size[1],int(x):int(x)+size[0]]=numpy.transpose(ind)#inverser car la matrice de buff l'est aussi
        self.spectro_plot.reset(self.variable)
        self.spectro_buffG_new.data=self.spectro_buffG.data*modif_spectro
        self.spectro_buffD_new.data=self.spectro_buffD.data*modif_spectro
        if self.variable.channel==0:
            data_plot=abs(self.spectro_buffG_new.data)
        else:
            data_plot=abs(self.spectro_buffD_new.data)
        self.spectro_plot.update_data(20*numpy.log10(data_plot.T),self.variable.db_min,self.variable.db_max)
        ################################################################
        #   Construction du nouveau signal temporel
        ################################################################
        BUFFER=self.variable.buffer_size
        taux_overlap=self.variable.covering
        decalage=int(BUFFER*(1-taux_overlap))
        freq_vect=numpy.zeros((BUFFER),'complex')
        nb_freq=numpy.size(self.spectro_buffG_new.data[:,0])
        self.signal_bufferG_new.data=numpy.zeros(numpy.shape(self.signal_bufferG.data))
        self.signal_bufferD_new.data=numpy.zeros(numpy.shape(self.signal_bufferD.data))      
        for ii in range(0,numpy.size(self.variable.data_time)):
            data=(BUFFER)*self.spectro_buffG_new.data[:,ii]#inverse normalisation (normalement freq=0 doit pas etre devisé par 2 en fft et ifft)
            freq_vect[0:nb_freq]=data
            freq_vect[-nb_freq+1::]=numpy.conj(numpy.fliplr([data[1:]])[0])     
            freq_vect=0.5*freq_vect
            freq_vect[0]=2*freq_vect[0]
            try:
                self.signal_bufferG_new.data[ii*decalage:ii*decalage+BUFFER]=numpy.sum([self.signal_bufferG_new.data[ii*decalage:ii*decalage+BUFFER],numpy.real(numpy.fft.ifft(freq_vect))],axis=0)
            except:
                pass
##############################################################################################            
            data=(BUFFER)*self.spectro_buffD_new.data[:,ii]#inverse normalisation (normalement freq=0 doit pas etre devisé par 2 en fft et ifft)
            freq_vect[0:nb_freq]=data
            freq_vect[-nb_freq+1::]=numpy.conj(numpy.fliplr([data[1:]])[0])     
            freq_vect=0.5*freq_vect
            freq_vect[0]=2*freq_vect[0]
            try:
                self.signal_bufferD_new.data[ii*decalage:ii*decalage+BUFFER]=numpy.sum([self.signal_bufferD_new.data[ii*decalage:ii*decalage+BUFFER],numpy.real(numpy.fft.ifft(freq_vect))],axis=0)
            except:
                pass
        max_sig0=numpy.amax(abs(self.signal_bufferG_new.data))
        max_sig1=numpy.amax(abs(self.signal_bufferD_new.data))
        div=max(max_sig0,max_sig1)
        if div>1:
            self.signal_bufferG_new.data=self.signal_bufferG_new.data/float(1.05*div)
            self.signal_bufferD_new.data=self.signal_bufferD_new.data/float(1.05*div)
        self.acqui=2

    def export_spectro_FFT_old(self):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_spectro_fft.txt',filter ='txt (*.txt *.)')
        try:
            fid=open(filename[0],'w')
            line='Freq (Freq values:line 1) vs Time (Time values:row 1) in dB/Hz\n'
            fid.write(line)
            if self.variable.channel==0:
                write_spectro(fid,20*numpy.log10(abs(self.spectro_buffG.data)/float(self.variable.delta_freq)),self.variable.data_freq,self.variable.data_time)
            else:
                write_spectro(fid,20*numpy.log10(abs(self.spectro_buffD.data)/float(self.variable.delta_freq)),self.variable.data_freq,self.variable.data_time)
            fid.close()
        except:
            pass
     
     
    def export_spectro_FFT(self,data):
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','result_spectro_fft.txt',filter ='txt (*.txt *.)')
        try:
            fid=open(filename[0],'w')
            line='Freq (Freq values:line 1) vs Time (Time values:row 1) in dB/Hz\n'
            fid.write(line)
            if self.variable.channel==0:
                write_spectro(fid,20*numpy.log10(abs(self.spectro_buffG_new.data)/float(self.variable.delta_freq)),self.variable.data_freq,self.variable.data_time)
            else:
                write_spectro(fid,20*numpy.log10(abs(self.spectro_buffD_new.data)/float(self.variable.delta_freq)),self.variable.data_freq,self.variable.data_time)
            fid.close()  
        except:
            pass
        
    def listen_sig_old(self):
        self.listen_old_button.setEnabled(False)
        app.processEvents()
        p=pyaudio.PyAudio()
        BUFFER=self.variable.buffer_size
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        stream = p.open(format=pyaudio.paFloat32,channels= 2,rate=RATE,input=False,output=True,frames_per_buffer=BUFFER)
        data=numpy.zeros((numpy.size(self.signal_bufferG.data),2))
        data[:,0]=self.signal_bufferG.data
        data[:,1]=self.signal_bufferD.data
        stream.write(data.astype(numpy.float32).tostring())
        stream.close()
        p.terminate()
#        QtCore.QThread.sleep(self.variable.time_max)
        self.listen_old_button.setEnabled(True)
        app.processEvents()
        
    def listen_sig(self):
        self.listen_button.setEnabled(False)
        app.processEvents()
        p=pyaudio.PyAudio()
        BUFFER=self.variable.buffer_size
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        stream = p.open(format=pyaudio.paFloat32,channels= 2,rate=RATE,input=False,output=True,frames_per_buffer=BUFFER)
        data=numpy.zeros((numpy.size(self.signal_bufferG_new.data),2))
        data[:,0]=self.signal_bufferG_new.data
        data[:,1]=self.signal_bufferD_new.data
        stream.write(data.astype(numpy.float32).tostring())
        stream.close()
        p.terminate()
#        QtCore.QThread.sleep(self.variable.time_max)
        self.listen_button.setEnabled(True)
        app.processEvents()
                 
    def save_wave(self):
        data=numpy.zeros((numpy.size(self.signal_bufferG_new.data),2))
        data[:,0]=numpy.multiply(self.signal_bufferG_new.data,32767).astype(numpy.int16)
        data[:,1]=numpy.multiply(self.signal_bufferD_new.data,32767).astype(numpy.int16)
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','cuttone.wav',filter ='wav (*.wav *.)')
        try:
            wave_file = wave.open(filename[0], 'w')
            wave_file.setparams((2, 2, RATE, 0, 'NONE', 'not compressed'))        
            for ii in range(0,numpy.size(data[:,0])):
                signal= wave.struct.pack('<hh', int(data[ii,0]),int(data[ii,1]))
                wave_file.writeframesraw( signal )
            wave_file.close()
        except:
            pass
        self.wave_button.setEnabled(True)
        
        
        
        
        
if __name__=='__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    splash=splashScreen()
    splash.show_splash()  
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    gui = spectro()
    gui.show()
    app.exec_()# -*- coding: utf-8 -*-  
    me = os.getpid()
    kill_proc_tree(me)
    